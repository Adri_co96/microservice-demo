package com.microservice.demo.adri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@SpringBootApplication
public class AdriApplication {
	
	@GetMapping("/weatherForecast")
	public String getMessage() {
		return "Hola Mundo desde Spring";
	}

	public static void main(String[] args) {
		SpringApplication.run(AdriApplication.class, args);
	}

}
